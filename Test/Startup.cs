﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Test.Startup))]
namespace Test
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            AreaRegistration.RegisterAllAreas();

            var container = ConfigureIoC(app);
            ConfigureGlobalFilters(GlobalFilters.Filters);
            ConfigureBundles(BundleTable.Bundles);
            ConfigureRoutes(RouteTable.Routes);
            
            ConfigureAuth(app, container);

            SeedDatabase(container);
        }
    }
}
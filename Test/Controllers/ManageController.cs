﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Test.Identity;
using Test.Models;

namespace Test.Controllers
{
    [Authorize(Roles = "MasterUser")]
    public class ManageController : Controller
    {
        private readonly ApplicationUserManager _userManager;

        public ManageController(ApplicationUserManager userManager)
        {
            if (userManager == null) throw new ArgumentNullException(nameof(userManager));
            _userManager = userManager;
        }

        public async Task<ActionResult> UserList()
        {
            return View(Map(await _userManager.GetAllUsersAsync()));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UserList(List<UserListItemViewModel> model)
        {
            if (ModelState.IsValid)
            {
                foreach (var user in model)
                {
                    if (user.Checked)
                    {
                        await _userManager.UpdateSecurityStampAsync(user.Id);
                    }
                }
                
                return RedirectToAction("UserList");
            }

            return View(model);
        }

        #region Mappers

        private List<UserListItemViewModel> Map(List<ApplicationUser> source)
        {
            return source.Select(m => new UserListItemViewModel
            {
                Id = m.Id,
                UserName = m.UserName
            })
            .ToList();
        }

        #endregion
    }
}
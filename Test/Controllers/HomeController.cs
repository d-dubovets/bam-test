﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using Test.Helpers;
using Test.Models;

namespace Test.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View(new HomeViewModel());
        }

        [ValidateAntiForgeryToken]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Index(HomeViewModel model)
        {
            if (ModelState.IsValid)
            {
                var key = AES256.GenerateKey();
                var encryptedData = AES256.Encrypt(model.Password, key);

                string publicKey;
                string privateKey;
                RSA2048.GenerateKeys(out publicKey, out privateKey);

                string encryptedAESKey = RSA2048.Encrypt(publicKey, key);
                string decryptedAESKey = RSA2048.Decrypt(privateKey, encryptedAESKey);

                model.EncryptedData = encryptedData;
                model.DecryptedData = AES256.Decrypt(encryptedData, decryptedAESKey);
            }

            return View(model);
        }

    }
}
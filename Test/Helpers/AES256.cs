using System;
using System.Security.Cryptography;
using System.Text;

namespace Test.Helpers
{
    public static class AES256
    {
        private static int _keySize = 256;

        public static string GenerateKey()
        {
            using (var aesEncryption = new RijndaelManaged())
            {
                aesEncryption.KeySize = _keySize;
                aesEncryption.BlockSize = 128;
                aesEncryption.Mode = CipherMode.CBC;
                aesEncryption.Padding = PaddingMode.PKCS7;
                aesEncryption.GenerateIV();

                var ivStr = Convert.ToBase64String(aesEncryption.IV);
                aesEncryption.GenerateKey();

                var keyStr = Convert.ToBase64String(aesEncryption.Key);
                var completeKey = ivStr + "," + keyStr;

                return Convert.ToBase64String(Encoding.UTF8.GetBytes(completeKey));
            }
        }

        /// <summary>
        /// Encrypt
        /// From : www.chapleau.info/blog/2011/01/06/usingsimplestringkeywithaes256encryptioninc.html
        /// </summary>
        public static string Encrypt(string iPlainStr, string iCompleteEncodedKey)
        {
            using (var aesEncryption = new RijndaelManaged())
            {
                aesEncryption.KeySize = _keySize;
                aesEncryption.BlockSize = 128;
                aesEncryption.Mode = CipherMode.CBC;
                aesEncryption.Padding = PaddingMode.PKCS7;

                aesEncryption.IV =
                    Convert.FromBase64String(
                        Encoding.UTF8.GetString(Convert.FromBase64String(iCompleteEncodedKey)).Split(',')[0]);

                aesEncryption.Key =
                    Convert.FromBase64String(
                        Encoding.UTF8.GetString(Convert.FromBase64String(iCompleteEncodedKey)).Split(',')[1]);

                byte[] plainText = Encoding.UTF8.GetBytes(iPlainStr);

                ICryptoTransform crypto = aesEncryption.CreateEncryptor();
                byte[] cipherText = crypto.TransformFinalBlock(plainText, 0, plainText.Length);

                return Convert.ToBase64String(cipherText);
            }
        }

        /// <summary>
        /// Decrypt
        /// From : www.chapleau.info/blog/2011/01/06/usingsimplestringkeywithaes256encryptioninc.html
        /// </summary>
        public static string Decrypt(string iEncryptedText, string iCompleteEncodedKey)
        {
            using (var aesEncryption = new RijndaelManaged())
            {
                aesEncryption.KeySize = _keySize;
                aesEncryption.BlockSize = 128;
                aesEncryption.Mode = CipherMode.CBC;
                aesEncryption.Padding = PaddingMode.PKCS7;
                aesEncryption.IV =
                    Convert.FromBase64String(
                        Encoding.UTF8.GetString(Convert.FromBase64String(iCompleteEncodedKey)).Split(',')[0]);

                aesEncryption.Key =
                    Convert.FromBase64String(
                        Encoding.UTF8.GetString(Convert.FromBase64String(iCompleteEncodedKey)).Split(',')[1]);

                ICryptoTransform decrypto = aesEncryption.CreateDecryptor();
                byte[] encryptedBytes = Convert.FromBase64CharArray(iEncryptedText.ToCharArray(), 0,
                    iEncryptedText.Length);

                return
                    Encoding.UTF8.GetString(decrypto.TransformFinalBlock(encryptedBytes, 0, encryptedBytes.Length));
            }
        }
    }
}
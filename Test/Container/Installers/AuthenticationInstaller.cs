﻿using System.Web;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Test.Identity;

namespace Test.Container.Installers
{
    public class AuthenticationInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<IAuthenticationManager>().UsingFactoryMethod((c) => HttpContext.Current.GetOwinContext().Authentication)
                .LifestyleTransient());

            container.Register(Component.For<RoleManager<IdentityRole>>().ImplementedBy<RoleManager<IdentityRole>>().LifestyleTransient());
            container.Register(Component.For<IRoleStore<IdentityRole, string>>().ImplementedBy<RoleStore<IdentityRole>>().LifestyleTransient());

            container.Register(Component.For<ApplicationUserStore> ().ImplementedBy<ApplicationUserStore>().LifestyleTransient());
            container.Register(Component.For<ApplicationUserManager>().ImplementedBy<ApplicationUserManager>().LifestyleTransient());
            container.Register(Component.For<ApplicationSignInManager>().ImplementedBy<ApplicationSignInManager>().LifestyleTransient());
        }
    }
}
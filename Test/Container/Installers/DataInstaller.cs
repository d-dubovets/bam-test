﻿using System.Data.Entity;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Test.Models;

namespace Test.Container.Installers
{
    public class DataInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<DbContext>().ImplementedBy<ApplicationDbContext>()
                    .DependsOn(Dependency.OnValue("connectionString", "DefaultConnection"))
                    .LifeStyle.Transient);
        }
    }
}
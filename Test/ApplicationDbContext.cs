﻿using Microsoft.AspNet.Identity.EntityFramework;
using Test.Identity;

namespace Test
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace Test.Models
{
    public class HomeViewModel
    {
        [Required]
        public string Password { get; set; }
        
        public string EncryptedData { get; set; }
        public string DecryptedData { get; set; }
    }
}
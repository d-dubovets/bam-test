﻿using System.Data.Entity;
using Castle.Windsor;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using Test.Identity;

namespace Test
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app, IWindsorContainer container)
        {
            app.CreatePerOwinContext(container.Resolve<DbContext>);
            app.CreatePerOwinContext(container.Resolve<ApplicationUserManager>);
            app.CreatePerOwinContext(container.Resolve<ApplicationSignInManager>);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider
                {
                    // Enables the application to validate the security stamp when the user logs in.
                    // This is a security feature which is used when you change a password or add an external login to your account.  
                    OnValidateIdentity = (async context =>
                    {
                        var claim = context.Identity.FindFirst("AspNet.Identity.SecurityStamp");
                        if (claim != null)
                        {
                            ApplicationUserManager userManager = null;
                            var userId = context.Identity.GetUserId();

                            try
                            {
                                userManager = container.Resolve<ApplicationUserManager>();
                                var user = await userManager.FindByIdAsync(userId);

                                if (user == null || string.IsNullOrWhiteSpace(user.SecurityStamp) == false && user.SecurityStamp != claim.Value)
                                {
                                    context.RejectIdentity();
                                }
                            }
                            finally
                            {
                                if (userManager != null)
                                {
                                    container.Release(userManager);
                                }

                            }
                        }
                    })
                }
            });
        }
    }
}
﻿using System.Web.Mvc;

namespace Test
{
    public partial class Startup
    {
        public static void ConfigureGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}

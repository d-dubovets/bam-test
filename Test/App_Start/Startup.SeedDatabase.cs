﻿using Castle.Windsor;
using Microsoft.AspNet.Identity;
using Test.Config;
using Test.Identity;

namespace Test
{
    public partial class Startup
    {
        public static void SeedDatabase(IWindsorContainer container)
        {
            var cfg = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
            var section = (InitAppConfigurationSection)cfg.Sections["InitAppConfigurationSection"];

            ApplicationUserManager userManager = null;

            try
            {
                userManager = container.Resolve<ApplicationUserManager>();

                for (var a = 0; a < section.Users.Count; a++)
                {
                    var buffer = section.Users[a];

                    var user = userManager.FindByEmail(buffer.Email);
                    if (user == null)
                    {
                        userManager.Create(new ApplicationUser { Email = buffer.Email, UserName = buffer.Email }, buffer.Password);
                        user = userManager.FindByEmail(buffer.Email);
                        userManager.AddToRole(user.Id, buffer.Role);
                    }
                }
            }
            finally 
            {
                if (userManager != null)
                {
                    container.Release(userManager);
                }
            }
        }
    }
}

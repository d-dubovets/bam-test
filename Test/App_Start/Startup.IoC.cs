﻿using System.Web.Mvc;
using Castle.Windsor;
using Owin;
using Test.Container;
using Test.Container.Installers;

namespace Test
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public IWindsorContainer ConfigureIoC(IAppBuilder app)
        {
            var container = new WindsorContainer()
                .Install(
                new DataInstaller(),
                new AuthenticationInstaller(),
                new ControllersInstaller()
                );

            var controllerFactory = new WindsorControllerFactory(container.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);

            return container;
        }
    }
}
using System.Data.Entity.Migrations;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Test.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Test.Models.ApplicationDbContext";
        }

        protected override void Seed(ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.
            context.Roles.AddOrUpdate(
                p => p.Name,
                new IdentityRole {Name = "MasterUser"},
                new IdentityRole {Name = "User"}
                );
        }
    }
}

using System.Configuration;

namespace Test.Config
{
    public class UserConfigurationElement : ConfigurationElement
    {

        [ConfigurationProperty("email", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string Email
        {
            get
            {
                return ((string)(base["email"]));
            }

            set
            {
                base["email"] = value;
            }
        }
        

        [ConfigurationProperty("role", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string Role
        {
            get
            {
                return ((string)(base["role"]));
            }

            set
            {
                base["role"] = value;
            }
        }

        [ConfigurationProperty("password", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string Password
        {
            get
            {
                return ((string)(base["password"]));
            }

            set
            {
                base["password"] = value;
            }
        }


    }
}
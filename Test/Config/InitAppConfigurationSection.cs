﻿using System.Configuration;

namespace Test.Config
{
    public class InitAppConfigurationSection: ConfigurationSection
    {
        [ConfigurationProperty("Users")]
        public UserElementCollection Users => ((UserElementCollection)(base["Users"]));
    }
}
﻿using System.Configuration;

namespace Test.Config
{
    [ConfigurationCollection(typeof(UserConfigurationElement))]
    public class UserElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new UserConfigurationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((UserConfigurationElement)(element)).Email;
        }

        public UserConfigurationElement this[int idx] => (UserConfigurationElement)BaseGet(idx);
    }
}
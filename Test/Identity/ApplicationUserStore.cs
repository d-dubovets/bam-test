using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Test.Models;

namespace Test.Identity
{
    public class ApplicationUserStore : UserStore<ApplicationUser>
    {
        public ApplicationUserStore(DbContext context)
            : base(context)
        {
        }

        public Task<List<ApplicationUser>> GetAllWithUserRoleAsync(string roleId)
        {
            return Users.Where(m => m.Roles.Any(r => r.RoleId == roleId)).ToListAsync();
        }
    }
}
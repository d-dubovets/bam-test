using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Test.Models;

namespace Test.Identity
{
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        public new ApplicationUserStore Store => (ApplicationUserStore) base.Store;
        public override bool SupportsUserSecurityStamp => true;

        public ApplicationUserManager(ApplicationUserStore store, RoleManager<IdentityRole> roleManager)
            : base(store)
        {
            if (roleManager == null) throw new ArgumentNullException(nameof(roleManager));
            _roleManager = roleManager;

            //todo in the real project I store this information in web.config
            UserValidator = new UserValidator<ApplicationUser>(this)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            // Configure user lockout defaults
            UserLockoutEnabledByDefault = true;
            DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            MaxFailedAccessAttemptsBeforeLockout = 5;
        }

        public async Task<List<ApplicationUser>> GetAllUsersAsync()
        {
            var role = await _roleManager.FindByNameAsync("User");
            return await Store.GetAllWithUserRoleAsync(role.Id);
        } 
    }
}